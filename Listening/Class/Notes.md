# 听力
## 总览
- Summarise spoken text & Write from dictation 比较重要
- WFD：同时给写作和听力供分。建议练习每天50道，练到完全无错。
- SST：对鸡精非常熟悉，尽量按照音频/回忆整理自己的内容点，保证语法和拼写分数。
- RS：听力和口语供分。前半句靠笔记，后半句靠记忆，youtube搜repeat sentence pte, 要求每天1h的训练强度。鸡精命中率不行，练能力为主。
- 剩下的题型一共30分，比较不太重要。

## SST (Summarise spoken text)
__注意：SST的时间是单独计算的，每题10分钟，尽量用满检查，不要提前切换__<br/>
WFD建议用时5分钟，注意留时间。

答题方法：
- 听的时候要写下主题
- keywords用连词或者序数词链接起来，要有逻辑性（firstly, then, in addition, finally）
- 用好程度副词
- 用he/she v.的形式作答然后连起来

答题要求：
- 找准四个点，写成四句话
- 65-70词
- 注意结构，用好逻辑连词
- 尽量使用听力原文的单词，不要换词
- 如果有正负两面的观点则两种观点都要写
- 用满10分钟检查
- 写简单句
- 如果例子是用来解释说明的话就不用包括，如果包含重要信息就写
- 笔记同时记，不确定是不是关键点的先记下来再筛查
- 鸡精主要是用来整理关键点，尽量不要用模板，因为占字数，可能没法覆盖关键点

练习材料
- 老托93
- 科学美国人（科学60秒）


## 多选题
一般5/6选2，7选3

## Select Missing Word

## HCS (Highlight Correct Summary)

## WFD
- 多写不扣分，所以不确定的时候可以不确定的词都写: this is a the very interesting book.
- 记每个单词的首字母
- books -> b', b'ed, b'g

The professor took a year off to work on her book.

