# 练习题

## SST
### Sea Creature

Wave power, oyster sits Ocean floor pass over it, NOV 2009 touch it generate on the shore
Device looks like snake ,
200 m long but prototypes are beting tested
System also like snake but  Power electrical generators, also tested
Supply sources of green energy

The topic was about the usage of wave power inspired by sea creature. Firstly, he suggested a device which sits on ocean floor and looks like a snake, the prototypes are being tested. After that, he mentioned there is also another system which is also like a snake, is used as power electrical generators and also being tested. Finally, he mentioned these could help supply sources of green energy. (69 words)

>Simulating sea creatures inspires many devices to generate green energy. Firstly, the device is like an oyster, which opens and closes as waves pass over it. And it has powered nine thousand home. Secondly, the device is a rubber tube like a snake, filled with water and floats below the surface. Thirdly, the device is also like a snake but made of steel and capable of generating electrical power.

### Energy Consumption

1kw every day - All forms of power consum
Phone charger use 100 of light blub all the time non stop
40 light bulb hot water
50 km 40 light bulb drive
125 light blub over time non stop

We use around 1 Kilowatt every day from all forms of power consumption. First of all, he mentioned phone chargers use 100 of light bulbs. Secondly, he suggested 40 light bulbs are used to heat water. Then he mentioned if we take average drive, which is 50 Kilometres, it will use 40 of light bulbs. Finally,  he concluded that in average we use 125 light bulbs.

>The speaker measures all forms of power consumption by using a 40-watt light bulb. Firstly, a phone charger only uses one-hundredth of light bulb. Secondly, the gas for heating uses forty light bulbs, which surprises the speaker. Thirdly, transportation is the biggest form of energy consumption, which uses a third of our energy. Finally, average British person is using 125 light bulbs of energy, which is huge.

### 故事叙述型（用因果、转折）
#### A Female Novelist

Nine -fiction for years
30 intention with fic detour
Not regret
Finally in earlty 90s, start fic
1 cha novel, 1992

The speaker has been writing fictions for years. She started writing at her age of 30 with the intention to write fictions. However, she had to make some detour. Fortunately, she was encouraged by another great writer and started writing fictions in early 90s. Finally, she wrote the 1st chapter of her novel in 1992. She loves writing fictions and never regret doing that. 

>The speaker wrote non-fictions for years, but secretly wanted to be a novelist. Although she has made a detour in writing non-fictions, she had no regret at all because she thought it was the right thing to do. Later she was inspired by a female novelist and started to write fiction. Therefore, she wrote “the secret life of bees”, which became the first chapter of her novel.

#### Indian Peasant
200 repis ,300 ro
Sea 1000-2000 repis per kg
The more they use mr
2000%
Global market
No Capital , can only buy expensive 

The debts for Indian peasants are so high. They come from the seeds which are 1000 to 2000 rupees per kilogram and the sprays that they need to use, and it has been increased by 2000% compared with last year because of globalisation. Unfortunately, these peasants have no capital, which means that they can only buy expensive seeds and sprays, and the more they use, the more they use.

>Indian peasants are becoming poorer and poorer because they have debts that are impossible to pay back. Peasants who have no money or capital have to borrow money from seed companies to buy seeds and pesticides. Additionally, they need to borrow more because of the overuse of pesticides or the raise of seeds' price. Therefore, seed companies become their major creditors resulting to a vicious cycle.

### 议论型
#### University Competition
For the best students, exercise
academic job market - English is a new land and English speaking countries are exposed to more intensive competitions
Through assessment of research quality and funding
Visible to vice chancellor

## FIB
gathering
permanently
tools
erosion
conolists

imported
mill
provided
availability
production

nervous
convey
~~sensoring~~ sensory
functions
~~reflexies~~ reflexes

state
struggle
source
engages
~~recruiment~~ recruitment
biased
originate

categorize
essentially
~~realistic~~ realists
lessons
mate

lots
~~attracted~~ tracked
travelling
habit
faithfully
flexible

uncovered
prestige
range
sequenced
nearly
~~brain~~ breeding

mimic
moved
rear
increased
nets
survival

trasmitted
echoes
~~glims~~ glimpse
~~they see~~ basically
appetite
floating

documented
incidentally
candidate
hauled
digestibility
~~conniverse~~ carnivorous

# WFD

The professor took a year off to work on her book.

The book was supported by many faculty members.

The new chemistry professor will deliver his first lecture tomorrow.

Much of the research is carried out in the laboratory.

The winter sun is lower but high enought to produce enough warmth.

The course dates are available on the colleage website.

Library plays an important role in student's ~~students'~~ life

There is a significant difference between theory and pratice in education.

Continuing students will be sent for application forms.

A balanced diet and regular exercise are necessary for good health.

A number of assignments will be gathered to the conference.

Science library is currently located on the ground of the library.

Take the first step to apply your university scholarship.

Growing population has posed the challenge to many governments.

Consumer confidence tends to increase as the economy expends.

Students should take advantge of online resources before attending the lecture.