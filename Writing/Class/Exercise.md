# SWT

## Researchers and 30 drug takers
### Keywords

- Thirty healthy adults performed a gambling task after receiving L-DOPA and a placebo respectively
- Required to choose between safe and risky options
- L-DOPA takers took more risky options and get happier after winning a small reward

### My answer
Researchers found that after asking thirty adults to perform a gambling task, which required test takers to choose between safe and risky options, after ~~receving~~ a L-DOPA and a placebo and ~~ask~~ for their happiness level, people who took L-DOPA took more risky options to get bigger rewards and got happier after winning a small reward.

### Modifications
- after开头，researchers found that放在后面。或者while 30 adults participated in a gambling experiment on happiness level which showed that...
- receiving和asking
- No matter how much of the reward

---

## Rosetta Stone
### Keywords
- was discovered in 1799, the carved characters that covered its surfaces was copied too much time, thus the surface of the stone accumulated many layers of material left over although tried to remove them. 
- Grease from human hands on exibitions.
- 1999 there was a chance to investigate and clean the stone to remove all and the original except a small square at the bottom left corner of ~~the face of~~ the stone ~~to show the darkened wax and the white infill~~

### Answer
There were many layers of materials accumulated on the surface of the Rosetta Stone which was discovered in 1799 since the carved characters were copied too much time until it was investigated and fully cleaned except a small square at the bottom left corner of the face of the stone at The British Museum in 1999.

---
### American Wines
### Keywords
- the national prohibition act (the volstead act) prohibited intoxicating liquors for beverage purpose but not for medical purpose
- nearly destroyed the indurstry
- fruit juice concentrate were produced and sold to other places in order to be made into wine until the government stepped in
- resulted in the halt of the American wine industry

### Answer
Since the National Prohibition Act, ~~which was also known as the Volstead Act~~, prohibited intoxicating liquors for beverage but not for ~~medical purposes and allowed anyone to make juice and cider~~ but not for the manufacture and sales of wine with medical and sacramental purposes as well as the juice and cider, so a loophole had been exploited to produce and sell fruit juice concentrate in order to produce illegal wines until the US government stepped in, which resulted in the halt of the American wine industry.

# Essay
## Artifical Intelligent robots
问同意哪种观点：
- body 1: 同意观点1
- body 2: 不同意观点2 - 他们没有意识到什么样的问题。it is just a matter of time before sth can do sth

>In retrospect, the phenomenon of more and more human jobs are taken by artificially intelligent robots has become a ubiquitous issue for debate. With the respect of whether robots will replace human workforce, people hold divergent view. From my perspective view, I believe that robots can only perform limited sorts of tasks. <br/>
First and foremost/On the one hand, it is believed that human workforce can be substituted by robots completely. This is because technology is developing rapidly and the robots can learn by themselves so that they can understand some complicated rules. For example, AlphaGo has defeated several top Go players. As a result, it is possible that robots are also able to do other kinds of complex tasks and take all human jobs.<br/>
In conclusion, having considered all the arguments above, I am firmly convinced that 改写论点1/论点2.

Additionally/On the other hand, 论点2. To be more precise, ... For instance, ... Therefore, 总结论点2.

### modifications:
- some people think that human labor will eventually become completely obsolete.
- outperform us on 比我们在……方面做得更好
- supersede

>Nowadays, whether human workforce will eventually be replaced with artifacially intelligent robots or not has attracted numerous controversies and drawn attention to the public. Undoubtedly, there are various consequences that will arise for the society. Some of these are beneficial, but some could cause issues. Personally, I believe that the robots are not able to perform certain types of tasks.

## Study-employment balance
>In retrospect, the phenomenon of the great difficulty of dealing with both study and employment altogether has become a ubiquitous issue for debate. With the respect of whether people could balance their study and employment well, people hold divergent view. From my perspective views, I believe that study could be done along with employment __due to__ ....

### Modifications:
agree/disagree的话要加上原因

## Cashless
- 优缺题：一定要2方面都要写，一段写ad一方面写disad
- B brings in just a few disadvantages, but if （足够小心）, 就可以享受它带来的好处。

>In retrospect, the phenomenon of the realization of cashless society has become a ubiquitous issue for debate. With the respect of whether it is ~~good~~ bless or ~~bad~~ curse that increasing number of peope are paying by cards instead of cash, people hold divergent views. From my perspective view, I believe that both benefits and drawbacks ~~can be brought by it~~ exist.


### Modifications
In retrospect, the phenomenon of increasing number of people using credit card instead of cash has become a ubiquitous issue for debate. With the respect of whether this phenomenon brings benefits or weaknesses，people hold divergent views. From my perspective of view, I believe that both of the positive and negative aspects exist when more people using credit cards.

### Give the solution
In retrospect, different kinds of problems that the governments and organizations are facing with has become a ubiquitous issue for debate. With the respect of which problem is the most serious and urgent, people hold divergent views. From my perspective view, I believe that air pollution shoud be reduced as much and as soon as possible ~~by developing public transportation and limiting the usage of non-recyclable materials, especially for containers~~ and the solutions are provided in the following.

>In conclusion, having considered all the arguments above, I am firmly convinced that global warming is the greatest environmental threat and the biggest challenge that humanity has ever faced. In order to address this issue, the government and our society should make a concerted effort, such as increasing energy efficiency, clean energy and protecting the world’s rainforests.