# Writing
## Overview
- Grammar, spelling, vocabulary, written discourse
- Spelling - 错一个扣一分，错两个就没分数了
- 英式拼写v美式拼写
    - Me,ue结尾的在美式英语里面删除
    - 英语中our结尾的美式单词变成or
    - re结尾的美式变成er
    - 英式英语ence结尾的时候美式变成ense
    - ise结尾的美式变成ize
    - 英语中ll的单词美式变成一个l， jeweller -> jeweler
    - Cheque - check; gaol - jail; sceptical - skeptical

## SWT
- 还给阅读供分
- 5 to 75字
- 10分钟

三种类型：
- 信息点
- 举例
- 拓展说明

***模板***
- Since, so, which...
- during, found that...

***模板常用连接词***
- Although
- Whereas
- 

***Tips***:
- 提出问题才有结果，所以对于研究结果需要包含问题内容(解释原因结果)
- 不要加入自己的观点
- 不要抄取整段的文章
- 直接说who did what
- 不要加入解释说明和扩展的内容
- 地点比时间重要

---
## Essay
- 200-300词

结构
- introduction: background + paraphrase + statement
    1. 改写背景句
    2. 简单提到side 1 & 2
    3. 给出自己的观点
- body 1
    1. 重新阐述side 1
    2. 给原因
    3. 给例子
    4. 总结side 1
- body 2
    1. 重新阐述side 2
    2. 给原因
    3. 给例子
    4. 总结side 2
- conclusion
    1. 改写背景句
    2. 强调自己的观点

做题方法
- 背模板！！！
- 熟悉作文题目，每个题目带2个观点
- 练习
- 字数推荐240-280

模板1：

Nowadays, whether 论点 or not has attracted numerous controversies and drawn attention to the public. Undoubtedly, there are various consequences that will arise for citizen. Some of these are beneficial, but some could cause issues. Personally, I believe that 论点1/论点2.

First and foremost/One the one hand, in terms of 论点1名词， a positive/negative consequence is that 解释论点1. For example, according to the Australian Bureau of Statistics, 数据例子。 As a result, 论点1总结。

Additionally/On the other hand, another main factor is 论点2名词。It is noteworthy that解释论点2. For instances, ... Therefore, 总结论点2.

In conclusion, I hold the view that 你的观点 because of 论点1名词/论点2名词。 In order to address this issue, the government and our society should make a concerted effort.

>citizen 可以替换成别的主体，比如gov, society, parents

模板2：
In retrospect, the phenomenon of 背景 has become a ubiquitous issue for debate. With the respect of whether 论点1/论点2， people hold divergent views. From my perspective view, I believe that 论点2/论点1.

First and foremost/On the one hand, 论点1. This is because ... For example, ... As a result, 总结论点1。

Additionally/On the other hand, 论点2. To be more precise, ... For instance, ... Therefore, 总结论点2.

In conclusion, having considered all the arguments above, I am firmly convinced that 改写论点1/论点2.
